const requestUrl = 'http://localhost:5000/api/user/login';
let token = '';


async function userLogin() {
    let login = document.getElementById("login").value;
    let password = document.getElementById("password").value;
    // function readLoginData() {
    //     let readData = {};
    //     readData["name"] = document.getElementById("name").value;
    //     readData["password"] = document.getElementById("password").value;
    //     return readData;
    // }
    const res = await fetch(requestUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json-patch+json'
        },
        body: JSON.stringify({
            login: login,
            password: password
        })
    }).then(res => res.json())
        .catch((err) => {
            console.log(err);
        });
    console.log(res);
    
    if (res && res.status) {
        Cookies.set('token', res.data.token)
        alert("Do want visit \"Product\" page?")
        window.open('http://127.0.0.1:5501/JavaScript/product/product.html')
    }
    else {
        alert("Please try again! Login or Password wrong!")
        console.log('Error...!');
    }
}

